package kz.rio.proxy

import akka.actor.ActorSystem
import akka.io.IO
import spray.can.Http
import spray.http.{HttpHeader, HttpHeaders, HttpRequest, Uri}
import spray.routing.{RequestContext, Route}

/**
 * Created by irybakov on 1/15/15.
 */
trait Proxy {

   def proxyRequest(updateRequest: RequestContext => HttpRequest)(implicit system: ActorSystem): Route =
    ctx => IO(Http)(system) tell (updateRequest(ctx), ctx.responder)

  private def stripHostHeader(headers: List[HttpHeader] = Nil) =
    headers filterNot (header => header is (HttpHeaders.Host.lowercaseName))

  private val updateUriUnmatchedPath = (ctx: RequestContext, uri: Uri) => uri.withPath(uri.path ++ ctx.unmatchedPath)

  def updateRequest(uri: Uri, updateUri: (RequestContext, Uri) => Uri): RequestContext => HttpRequest =
    ctx => ctx.request.copy(
      uri = updateUri(ctx, uri),
      headers = stripHostHeader(ctx.request.headers))

  def proxyToUnmatchedPath(uri: Uri)(implicit system: ActorSystem): Route = proxyRequest(updateRequest(uri, updateUriUnmatchedPath))

}
