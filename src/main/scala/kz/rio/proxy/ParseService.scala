package kz.rio.proxy


import akka.util.Timeout
import spray.http.{Uri, StatusCodes, HttpResponse}
import spray.routing._
import scala.concurrent.duration._
import scala.util.{Try, Success, Failure}

/**
 * Created by irybakov on 1/17/15.
 */
class ParseService extends HttpServiceActor with Proxy{
  implicit val timeout: Timeout = Timeout(30 seconds)
  implicit val marshaller = spray.http.HttpEntity

  implicit val system = context.system

  //val uri = "http://www.avenue32.com"//"http://spray.io"

  val uri = "http://logex.kz"
  val proxyRoute: Route = proxyToUnmatchedPath(uri)

  val route: Route = {
    path("about") {
      get{
        complete("Proxy server implemented with Akka and Spray")
      }
    }

  }

  //connector.ask(ctx.request).mapTo[HttpResponse]

  def receive: Receive = runRoute(route ~ proxyRoute)
}