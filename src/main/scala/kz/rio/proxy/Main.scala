package kz.rio.proxy

import akka.actor.{Props, ActorSystem}
import akka.event.Logging
import akka.io.IO
import akka.util.Timeout
import akka.pattern.ask
import spray.can.Http
import spray.can.Http.ClientConnectionType
import scala.concurrent.duration._
import spray.http._

import scala.util.{Failure, Success}

/**
 * Created by irybakov on 1/17/15.
 */
object Main extends App with HostLevelProxy{
  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("simple-spray-client")
  implicit val timeout = Timeout(50 seconds)

  // execution context for futures belows
  val log = Logging(system, getClass)
  val host = "spray.io"
  //val site = "apples.kz"


  val service = system.actorOf(Props(new ParseService()))
  IO(Http) ! Http.Bind(service, "0.0.0.0", port = 8082)
  /*
  val result = for {
    result2 <- proxyHostLevel(host)
    //result3 <- demoRequestLevelApi(host)
  } yield Set(result2)

  result onComplete {
    case Success(res) => log.info("{} is running {}", host, res mkString ", ")
    case Failure(error) => log.warning("Error: {}", error)
  }
  result onComplete { _ => system.shutdown() }
  */


}
